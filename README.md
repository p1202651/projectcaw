# projectCAW

Projet de Conception d'Application Web - M1INF03 

Toutes les features fonctionnent, néanmoins, et je m'en suis rendu compte au dernier moment, j'ai oublié de faire des git add pour certaines questions. Les fichiers crées pour ces question n'apparaissent qu'au commit 3.2 ...

## TP3
Le plugin maven est utilisé pour la mise en place de l'intégration continue.

### 2.1 Pattern contexte
La méthode init() de la servlet Init a été modifiée afin de créer nos instances de DAOs `gestion de passage` de liste d'`User` et de `Salle` et de la ajouter dans le contexte applicatif.
Elles sont récupérées par les jsp de cette manière :

```java
<jsp:useBean id="passages" type="fr.univlyon1.m1if.m1if03.classes.GestionPassages" scope="application"/>
<jsp:useBean id="salles" type="java.util.Map<java.lang.String, fr.univlyon1.m1if.m1if03.classes.Salle>" scope="application"/>
<jsp:useBean id="users" type="java.util.Map<java.lang.String,fr.univlyon1.m1if.m1if03.classes.User>" scope="application"/>
```

### 2.2 Pattern chaine de responsabilité
#### 2.2.1 Authentification
Un filtre `AccessFilter` a été créé et permet de redirriger tout utilisateur non loggué au formulaire de connexion. 
Ce filtre va laisser passer les requêtes vers le formulaire de connexion.

#### 2.2.2 Autorisation
Un second filtre `AdminFilter` a été créé afin d'interdire l'accès à la partie administrateur du site pour tout utilisateur ne possédant pas les droits requis.

Le premier filtre appellé est `AccesFilter` (sur toutes les parties du site) et le second est `AdminFilter` (uniquement lors de l'appel d'interface_admin.jsp).

## TP4

Nous avons eu beaucoup de mal sur ce TP, entre MIF10 à rendre, la vague de CC du début de semaine, etc ...

La partie 1 a été réalisée, pour le reste ....

Nous n'avons compris que très tard comment l'API devait fonctionner. Les filtres sont inneficaces, seules les méthodes de users et salles fonctionnent selon swagger ...

## TP5

Toute la partie 2 a été réalisée, nous avons toujours un problème avec notre VM et ne pouvons pas y accéder.

## TP7 MIF03

### Q1

- le temps de chargement de la page HTML initiale : 179ms, 176ms, 202ms => 185ms
- le temps d'affichage de l'app shell : 184ms, 186ms, 202ms => 190ms
- le temps d'affichage du chemin critique de rendu (CRP) : 199ms, 187ms, 207ms => 197 ms

### Q2

- le temps de chargement de la page HTML initiale : 128ms, 145ms, 158ms => 143ms  (29% amélioration)
- le temps d'affichage de l'app shell : 149ms, 147ms, 165ms => 153ms (24% amélioration)
- le temps d'affichage du chemin critique de rendu (CRP) : 156ms, 151ms, 166ms => 157ms (25% amélioration)

### Q3

Nous avions deja utilisé un CDN en amont de ce TP (Bootstrap)

Le premier rapport LightHouse effectué indiquait 86/100 en performance.
Une fois l'ajout de balise `defer` sur les les scripts Bootstrap et Ajax,
le déplacement du CDN mustache entre le body et les templates mustache le rapport indique 100/100.

Les valeurs des temps de chargement ou d'affichage restent sensiblement les mêmes








