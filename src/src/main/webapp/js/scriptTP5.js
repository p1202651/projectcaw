/*
* Variables globales
*/
let URL = "https://192.168.75.61/api/v3";
let token;
let tkn;
let login;

/*
* Menu (toggle)
*/
    $('#titreMenu').click(() => {
        $('#menu').slideToggle();
    });

/*
* Donnees Mustache
*/
    let mockObject = {
        compte : {
            login: 'Aym',
            name: 'Aymeric',
            admin: false
        },
        index: {
            salle: "C4",
            entree: Date.now(),
            sortie: Date.now()
        },
        passages: {
            salle: "C2",
            entree: Date.now(),
            sortie: Date.now()
        }
    };

/*
* Routage
*/
    let templates = [ "#monCompte-template", "#index-template", "#passages-template"];
    window.addEventListener('hashchange', () => {
        let hash = window.location.hash;
        let target = hash.replace('#', '').toString();
        if(templates.indexOf(`${hash}-template`) >= 0) {
            if (target === "monCompte") {
                console.log("moncompte")

                $.ajax({
                    type: "GET",
                    url: URL + `/users/${login}`,
                    dataType: "json",
                    headers: {"Authorization": `${tkn}`},
                })
                    .done((res, textStatus, request) => {
                        buildTemplate(`${hash}-template`, res, hash);
                    });


            } else if (target === "index") {
                console.log("index")
                $.ajax({
                    type: "GET",
                    url: URL + `/passages/byUser/${login}/enCours`,
                    headers: {"Authorization": `${tkn}`, "Accept" : "application/json"},
                    dataType: "json"
                })
                    .done((data) => {
                        if (data.length > 0) {
                            let salles = [];
                            for (let i = 0; i<data.length; i++) {
                                let id = data[i].replace('passages/','');

                                $.ajax({
                                    type: "GET",
                                    url: URL + `/passages/${id}`,
                                    dataType: "json",
                                    headers: {"Authorization": `${tkn}`},
                                })
                                    .done((data) => {
                                        salleId = data.salle.replace('salles/', '');
                                        $.ajax({
                                            type: "GET",
                                            url: URL + `/salles/${salleId}`,
                                            dataType: "json",
                                            headers: {"Authorization": `${tkn}`},
                                        })
                                            .done((data) => {
                                                let dataToReturn = {
                                                    salle: data.nom
                                                }

                                            console.log("data2 : ");

                                            salles.push(data);
                                            console.log(salles);
                                            buildTemplate(`${hash}-template`, salles, hash);
                                        } )

                                    });
                            }
                        }
                    });

            } else if (target === "passages") {
                console.log("passages")
                $.ajax({
                    type: "GET",
                    url: URL + `/passages/byUser/${login}`,
                    dataType: "json",
                    headers: {"Authorization": `${tkn}`},
                })
                    .done((data) => {
                        if (data.length > 0){
                            let passages = [];
                            for (let i = 0; i<data.length; i++) {
                                let id = data[i].replace('passages/','');

                                $.ajax({
                                    type: "GET",
                                    url: URL + `/passages/${id}`,
                                    dataType: "json",
                                    headers: {"Authorization": `${tkn}`},
                                })
                                    .done((data) => {
                                        dataToReturn = {
                                            salle : data.salle.replace('salles/', ''),
                                            user : data.user.replace('users/', ''),
                                            dateEntree: data.dateEntree,
                                            dateSortie: data.dateSortie
                                        }
                                        passages.push(dataToReturn);
                                        buildTemplate(`${hash}-template`, passages, hash);
                                        console.log("data : " + dataToReturn);
                                    });
                            }
                        }
                    });
            }

        }
        console.log("hash : " + hash);
        show(hash);
    });

    function buildTemplate(script, data, elt) {
        let template = $(script).html();
        Mustache.parse(template);
        let rendered = Mustache.render(template, data);
        console.log("crowd data : " + data);
        $(`${elt} ul`).html(rendered);
    }

/*
* Connexion
*/
let loginForm = document.forms.namedItem("login-form");
$('#login-form').on('submit', function(e) {
    e.preventDefault();
    let fData = new FormData(loginForm);
    fData.append('nom', '');
    fData.append('admin', true);
    let data = JSON.stringify(Object.fromEntries(fData));

    $.ajax({
        type: "POST",
        url: URL + "/users/login",
        contentType: "application/json",
        data: data,
        headers: {"Content-Type": "application/json", "Authorization": `${tkn}`},
        dataType: "json",

    })
        .done((data, textStatus, request) => {
            tkn = request.getResponseHeader("authorization");
            token = tkn.replace("Bearer ", "");
            login = $("#user-login").val();
            window.location.assign(window.location.origin + "/v3/#monCompte");
            $("#login-form").hide();
            $(".salle").show();
        });
});

/*
* Déconnexion
*/
$('#deco').on('submit', function(e) {
    e.preventDefault();
    $.ajax({
        type: "POST",
        url: URL + "/users/logout",
        contentType: "application/json",
        headers: {"Content-Type": "application/json", "Authorization": `${tkn}`},
        dataType: "json",

    })
        .done(() => {
            window.location.assign(window.location.origin + "/v3/#index");
            login = null;
            token = null;
            tkn = null;
            $("#login-form").show();
            $(".salle").hide();
        });
});

/*
* Changer nom
*/
$('#nom-form').on('submit', function(e) {
    e.preventDefault();
    let nomForm = document.forms.namedItem("nom-form");
    let fData = new FormData(nomForm);
    let data = JSON.stringify(Object.fromEntries(fData));

    $.ajax({
        type: "PUT",
        url: URL + `/users/${login}/nom`,
        contentType: "application/json",
        headers: {"Authorization": `${tkn}`},
        dataType: "json",
        data: data
    })
        .done(() => {
            window.location.assign(window.location.origin + "/v3/#monCompte");
        });
});

/*
* Entrer
*/
$('#entrer-form').on('submit', function(e) {
    e.preventDefault();

    let entreeForm = document.forms.namedItem("entrer-form");
    let fData = new FormData(entreeForm);
    fData.append("user", login);

    let data = JSON.stringify(Object.fromEntries(fData));

    $.ajax({
        type: "POST",
        url: URL + `/passages`,
        contentType: "application/json",
        headers: {"Authorization": `${tkn}`, "Content-Type" : "application/json"},
        data: data
    })
        .done(() => {
            window.location.assign(window.location.origin + "/v3/#index");
        });
});


/*
* Sortie
*/
$('#sortir-form').on('submit', function(e) {
    e.preventDefault();

    let sortirForm = document.forms.namedItem("sortir-form");
    let fData = new FormData(sortirForm);
    fData.append("user", login);
    fData.append("dateSortie", "now");

    let data = JSON.stringify(Object.fromEntries(fData));

    $.ajax({
        type: "POST",
        url: URL + `/passages`,
        contentType: "application/json",
        headers: {"Authorization": `${tkn}`, "Content-Type" : "application/json"},
        data: data
    })
        .done(() => {
            window.location.assign(window.location.origin + "/v3/#index");
        });
});

/*
* Gestion affichage
*/
    function show(hash) {
        console.log("display");
        $('.active')
            .removeClass('active')
            .addClass('inactive');
        $(hash)
            .removeClass('inactive')
            .addClass('active');
    }


/*
* Edition
*/
    $('input').attr("contentEditable", "true");






